from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client

from .models import PostStatus
from .forms import FormStatus
from django.utils import timezone

from . import views

import time 

from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class SampleTest(TestCase):
    # test eksistensi landing page
    def test_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.stat)

    def test_status_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')

    def test_dummypage(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code,404)

    def test_create_new_model(self):
        new_status = PostStatus.objects.create(status ="Saya senang", date=timezone.now())

        counting_all_status = PostStatus.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form_is_post(self):
        response = Client().post('/', data={'status' : 'Kabar saya baik', 'date' : '2019-10-30 18:00'})
        self.assertEqual(response.status_code, 302)

    def test_is_valid(self):
        response = FormStatus(data={'status' : 'Kabar saya baik', 'date' : '2019-10-30 18:00'})
        self.assertTrue(response.is_valid())

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_status_is_post(self):
         selenium = self.selenium
         selenium.get(self.live_server_url)
         text_area = selenium.find_element_by_id("status")
         text_area.send_keys("Coba Coba")
         selenium.find_element_by_id("submit").send_keys(Keys.RETURN)
         self.assertIn("Coba Coba",selenium.page_source)